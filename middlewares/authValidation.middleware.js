const express = require("express");
const router = express.Router();
var respObj = require("../helpers").respObj;
router.use((req, res, next) => {	
	if (!req.isAuthenticated()) {
		return res.status(500).json(
			respObj.badRequest("Login to access this content")
		);
	} else {
		next();
	}
});

module.exports = router;