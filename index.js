// To Set Environt varibales 
const dotenv = require('dotenv').config();
const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const http = require('http');
//const https = require('https');
const url = require('url')
const path = require('path');
const passport = require('passport');
const uuid = require('uuid/v4');
//const bodyParser = require("body-parser");
const app = express();
app.use(express.json())
global.BASE_URL = process.env.BASE_URL || 'http://localhost:4000';
//Configure isProduction variable
global.isProduction = process.env.NODE_ENV === 'production';
// Passport Config
require('./config/passport')(passport);
// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(
    db, {
      useNewUrlParser: true
    }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// initializing models
require("./models");
const host = process.env.APP_HOST || "localhost";
// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, '/public')));
// Express body parser
app.use(express.urlencoded({
  extended: true
}));

// Express session
app.use(
  session({
    genid: (req) => {
      console.log('Inside the session middleware');
      console.log(req.sessionID);
      return uuid(); // use UUIDs for session IDs
    },
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);
// Passport middleware
app.use(passport.initialize());
app.use(passport.session());
// Connect flash
app.use(flash());

// Global variables
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

// Routes
const router = require("./routes");
app.use('/', router);
const PORT = process.env.PORT || 4000;
app.listen(PORT, console.log(`Server started on port ${PORT}`));