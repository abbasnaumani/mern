const express = require('express');
const router = express.Router();
// const countriesRoutes = require('./');
// router.use ('/country', countriesRoutes);
//router.use(apiUnAuthorizedRoutes);
//router.get('/login', (req, res) => res.render('login'))
const loginController = require("../../../controllers/index").api.login;
router.get("/all_user", loginController.getAllUsers);
module.exports = router;