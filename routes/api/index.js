const express = require('express');
const router = express.Router();

var apiAuthorizedRoutes = require("./authorized");
var apiUnuthorizedRoutes = require("./unauthorized");
var md = require('../../middlewares');
router.use(apiUnuthorizedRoutes);
//router.use(apiAuthorizedRoutes);
router.use('/', md.authValidation, apiAuthorizedRoutes);


module.exports = router;