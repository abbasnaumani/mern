const express = require('express');
const router = express.Router();

const loginController = require("../../../controllers/index").api.login;
router.get("/all_user", loginController.getAllUsers);
router.get('/dashboard', (req, res) => res.render('dashboard'));
module.exports = router;