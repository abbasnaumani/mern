var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
// Load User model
const User = require('../models/user');
const UserController = require('../controller/user');
var db = mongojs('mongodb://mytasklist_user:mytasklist_user1@ds033669.mlab.com:33669/mytasklist_dev', ['tasks','users'])

// Get All Tasks
router.get('/users', function (req, res, next) {
    db.users.find(function (err, users) {
        //if (err) throw err;
        if (err) {
            res.send(err);
        }
        res.json(users);
    });
});



// Get All Tasks
router.get('/tasks', function (req, res, next) {

    UserController.getUsers(function(err, results){
        
    });
    // db.tasks.find(function (err, tasks) {
    //     //if (err) throw err;
    //     if (err) {
    //         res.send(err);
    //     }
    //     res.json(tasks);
    // });
});
// Get Single Task
router.get('/task/:id', function (req, res, next) {
    db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, function (err, task) {
        //if (err) throw err;
        if (err) {
            res.send(err);
        }
        res.json(task);
    });
});
// Save Single Task
router.post('/task', function (req, res, next) {
    var task = req.body;
    if (!task.title || (task.isDone + '')) {
        res.status(400);
        res.json({'error': "Bad Data"});
    } else {
        db.tasks.save(task, function (err, task) {
            if (err) {
                res.send(err);
            }
            res.json(task);
        });
    }
});
// Delete Task
router.delete('/task/:id', function (req, res, next) {
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, function (err, task) {
        if (err) {
            res.send(err);
        }
        res.json(task);
    });
});
// Update Task
router.put('/task/:id', function (req, res, next) {
    var task = req.body;
    var updateTask = {};
    if (task.isDone) {
        updateTask.isDone = task.isDone;
    }
    if (task.title) {
        updateTask.title = task.title;
    }
    if (!updateTask) {
        res.status(400);
        res.json({'error': "Bad Data"});
    } else {
        db.tasks.update({_id: mongojs.ObjectId(req.params.id)}, updateTask, {}, function (err, task) {
            if (err) {
                res.send(err);
            }
            res.json(task);
        });
    }
});
module.exports = router;