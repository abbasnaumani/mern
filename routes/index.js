const express = require("express");
const router = express.Router();

// const firebaseMiddleware = require("express-firebase-middleware");

var apiRoutes = require("./api/index");
var webRoutes = require("./web/index");
//const User = require('../models/users');
const mongoose = require('mongoose');
//var User = require('../../models/index').User;
var User = mongoose.model("User");
router.use("/api",apiRoutes);
router.use(webRoutes);
router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
  });
module.exports = router;
