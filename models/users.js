const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//var timestamps = require("mongoose-timestamp");

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

//userSchema.plugin(timestamps);
// Display Genre create form on GET.
const userModel = mongoose.model('User', UserSchema);
// var testMethod = function (req, res, next) {
//   return {'error': "Bad Data"};
  
// };

module.exports = userModel;
// module.exports = {
//   userModel
// };