var mongoose = require("mongoose");
var User = mongoose.model("User");

var getData = function () {
    return new Promise((resolve, reject) => {
        var userPromise = User.find();
        userPromise
            .then(data => {
                resolve(data);
            })
            .catch(reject);
    })
};

var updateData = (id, data) => {
    return new Promise((resolve, reject) => {
        User.find().then(result =>{
            if(result.length == 0){
                User.create(data).then(result => {
                    resolve(result);
                })
                    .catch(error=>{
                        reject(error);
                    });
            }
            else {
                console.log("updating data");
                User.findByIdAndUpdate(id, { $set: data},{new:true}).then(data =>{
                    resolve(data);
                })
                    .catch(error=>{
                        reject(error);
                    })
            }
        })
    });
};


module.exports = {
    getData,
    updateData
};