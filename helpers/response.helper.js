var success = (message, data = null, total = null) => {
	var resultObj = {
		status: "success",
		message: message,
		data: data,
		redirect_to: data.redirect_to
	};
	if (total !== null) {
		resultObj.total = total;
	}
	return resultObj;
};

var badRequest = (message, data = null) => {
	return {
		status: "failure",
		message: message,
		data: data
	};
};

var internalError = (error) => {
	return {
		status: "failure",
		message: "Sorry! request could not be completed",
		data: error && error.message ? error.message : error
	};
};


module.exports = {
	success,
	badRequest,
	internalError
};