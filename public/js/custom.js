$(document).ready(function () {
    //alert(baseURL);
});

/*
 * ## Type can be either error, success, warning Or info
 * ## Content will show the Message to display
 * ## Title is the heading of Message if any
 * ## TimeOut in seconds
 * */
function notificationAlert(type, content, title, timeOut) {
    if (type == '' || typeof (type) == "undefined") {
        type = 'error';
    }
    if (content == '' || typeof (content) == "undefined") {
        content = 'You Got Error';
    }
    if (title == '' || typeof (title) == "undefined") {
        title = '';
    }
    if (timeOut == '' || typeof (timeOut) == "undefined") {
        timeOut = 5; // in seconds
    }
    timeOut = timeOut * 1000;
    /*// by Default Toastr accept time in Micro Seconds so multiplying by 1000*/

    content = content.replace(/_/g, ' ');
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": timeOut,
        "extendedTimeOut": timeOut,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    switch (type) {
        case 'success':
            toastr.success(content, title, {
                timeOut: timeOut
            });
            break;
        case 'error':
            toastr.error(content, title, {
                timeOut: timeOut
            });
            break;
        case 'info':
            toastr.info(content, title, {
                timeOut: timeOut
            });
            break;
        case 'warning':
            toastr.warning(content, title, {
                timeOut: timeOut
            });
            break;
    }
}

function validateFields(formId) {
    var fields = $("#" + formId + " :input").serializeArray();
    var error = [];
    var skipArray = ['action'];
    var emailArray = ['email', 'notification_email', 'secondary_notification_email'];
    var passwordArray = ['password', 'confirm_password'];
    var skipforEmpty = ['secondary_notification_email', 'cc_email', 'bcc_email'];
    var fname = 'no_name';
    var passwordErr = false;
    var password = '';
    var confirm_password = '';
    /*var regexy = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;*/
    var regexy = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $.each(fields, function (i, field) {
        fname = field.name;
        if ($.trim(fname) == 'password') {
            password = $.trim(field.value);
        }
        if ($.trim(fname) == 'confirm_password') {
            confirm_password = $.trim(field.value);
        }
        if ($.inArray(fname, skipArray) == -1) {
            // console.log(fname);
            // console.log($.inArray(fname, emailArray));
            if ($.trim(field.value) == '') {
                //error.push( { fname :  'Please enter '+fname} );
                if ($.inArray(fname, skipforEmpty) == -1) {
                    var myregexp = /\[(.*?)\]/;
                    var match = myregexp.exec(fname);
                    if (match != null) {
                        fname = match[1];
                    }
                    error[i] = 'Please enter ' + fname;
                }
            } else if ($.inArray(fname, passwordArray) > -1) {
                let plength = $.trim(field.value);
                if (plength < 6) {
                    error[i] = fname + ' must be at least 6 characters';
                } else if ($.trim(fname) == 'confirm_password' && $.trim(password) != '' && $.trim(confirm_password) != '' && $.trim(password) != $.trim(confirm_password) && !passwordErr) {
                    passwordErr = true;
                    error[i] = 'Password & Confirm Password field should be same.';
                }
            } else if ($.inArray(fname, emailArray) > -1) {
                if (!regexy.test(field.value)) {
                    error[i] = 'Please enter correct format of email (example@example.com)';
                }
            }
        }
    });
    return error;
}

function showErrors(errors) {
    var msg = '';
    var error = '';
    $.each(errors, function (i, val) {
        if (errors[i] != '' && typeof (errors[i]) != "undefined") {
            error = errors[i] + '<br>';
            msg += error.replace(/_/g, ' ').toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });
        }
    });
    if (msg != '') {
        notificationAlert('error', msg);
        //bsAlert(msg, 'alert-danger', 'alert_placeholder');
    }
}

function reload_page(url) {
    location.href = baseURL + url;
}

function validateFieldsByFormId(formId) {
    var error = validateFields(formId);
    var action = $('#' + formId + '_action').val();
    var errorMsg = '';
    var flag = true;
    if (error.length > 0) {
        showErrors(error);
        flag = false;
    }
    if (flag) {
        $.ajax({
            type: "POST",
            url: baseURL + action,
            data: $('#' + formId).serialize(),
            dataType: "json",
            success: function (data) {
                if (data.status == 'success') {
                    console.log(data.data);
                    notificationAlert('success', data.message, 'Success!');
                    //  bsAlert(data.message, 'alert-success', 'alert_placeholder');
                    if (data.redirect_to != '' && typeof (data.redirect_to) != "undefined") {
                        setTimeout(reload_page(data.redirect_to), 4000);
                    }
                } else {
                    notificationAlert('error', data.message, 'Inconceivable!');
                    //  bsAlert(data.message, 'alert-danger', 'alert_placeholder');
                }
            },
            error: function (data) {
                // Error...
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (i, val) {
                    let objectErrors = '';
                    let stringErrors = '';
                    if (errors[i] != 'undefined' && errors[i] != null) {
                        if (typeof errors[i] === 'object' && typeof errors[i].message != 'undefined') {
                            objectErrors += errors[i].message + '<br>';
                        } else if (typeof errors[i] === 'string') {
                            stringErrors += errors[i] + '<br>';
                        }
                    }
                    if (objectErrors != '') {
                        errorMsg = objectErrors;
                        errorMsg
                    } else if (stringErrors != '') {
                        errorMsg = stringErrors;
                    }
                });
                notificationAlert('error', errorMsg, 'Inconceivable!');
                //  bsAlert(errorMsg, 'alert-danger', 'alert_placeholder');
            }
        });
    }
}