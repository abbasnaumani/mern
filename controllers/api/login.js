const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var User = mongoose.model("User");
var respObj = require("../../helpers").respObj;
// Get All Users
var getAllUsers = (req, res) => {
    console.log(" In Get all users Method.........");
    User.find(function (err, users) {
        //if (err) throw err;
        if (err) {
            res.send(err);
        }
        res.json(users);
    });
};

var login = async (req, res, next) => {
    const {
        email,
        password
    } = req.body;
    var data = new Object();
    console.log(BASE_URL);
    console.log("email" + email);
    // if (req.method === 'POST') {}
    console.log(req.sessionID);    

    passport.authenticate('local', (err, user, info) => {
        if (info) {
            return res.status(500).json(respObj.success(info.message));
        }
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(200).json(respObj.badRequest("That email is not registered", null));
        }
        req.login(user, (err) => {
            if (err) {
                return next(err);
            }
            data.user_data = user;
            data.redirect_to = '/dashboard';
            return res.status(200).json(respObj.success("login successfully", data));
        })
    })(req, res, next);

    // Match user
    // User.findOne({
    //     email: email
    // }).then(user => {
    //     if (!user) {
    //         res.status(200).json(respObj.badRequest("That email is not registered", null));
    //     } else {
    //         bcrypt.compare(password, user.password, (err, isMatch) => {
    //             if (err) throw err;
    //             if (isMatch) {
    //                 res.status(200).json(respObj.success("login successfully", user));
    //             } else {
    //                 res.status(200).json(respObj.badRequest("Password is incorrect", null));
    //             }
    //         });
    //     }
    // }).catch(err => res.status(500).json(respObj.internalError(err)));
}
var register = async (req, res, next) => {
    const {
        name,
        email,
        password,
        confirm_password
    } = req.body;
    console.log("name: " + name + " | email: " + email + " | password: " + password + " | confirm_password" + confirm_password);
    console.log(req.sessionID);
    var data = new Object();
    let errorMsg = '';


    if (!name || !email || !password || !confirm_password) {
        errorMsg = 'Please enter all fields';
    }

    if (password != confirm_password) {
        errorMsg = 'Passwords do not match';
    }

    if (password.length < 6) {
        errorMsg = 'Password must be at least 6 characters';
    }

    if (errorMsg != '') {
        res.status(200).json(respObj.badRequest(errorMsg, null));
    } else {
        User.findOne({
            email: email
        }).then(user => {
            if (user) {
                res.status(200).json(respObj.badRequest('Email already exists', null));
                // //data.push({ user_data: user });
                // data.user_data = user;
                // data.redirect_to = '/login';
                // console.log(data);
                // res.status(200).json(respObj.success("You are now registered and can log in", data));
            } else {
                const newUser = new User({
                    name,
                    email,
                    password
                });
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        newUser
                            .save()
                            .then(user => {
                                req.flash(
                                    'success_msg',
                                    'You are now registered and can log in'
                                );
                                data.user_data = user;
                                data.redirect_to = '/login';
                                res.status(200).json(respObj.success("You are now registered and can log in", data));
                            }).catch(err => res.status(500).json(respObj.internalError(err)));
                    });
                });
            }
        }).catch(err => res.status(500).json(respObj.internalError(err)));
    }
}
module.exports = {
    getAllUsers,
    login,
    register
};